<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TicketStatusesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ticket Statuses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-statuses-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ticket Statuses', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'status_id',
            'status_title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
