<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TicketStatuses */

$this->title = 'Create Ticket Statuses';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-statuses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
