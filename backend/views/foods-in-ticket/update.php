<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FoodsInTicket */

$this->title = 'Update Foods In Ticket: ' . ' ' . $model->food_in_ticket_id;
$this->params['breadcrumbs'][] = ['label' => 'Foods In Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->food_in_ticket_id, 'url' => ['view', 'id' => $model->food_in_ticket_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="foods-in-ticket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
