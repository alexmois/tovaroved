<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FoodsInTicketSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foods-in-ticket-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'food_in_ticket_id') ?>

    <?= $form->field($model, 'ticket_id') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'food_id') ?>

    <?= $form->field($model, 'package_id') ?>

    <?php // echo $form->field($model, 'food_in_ticket_count') ?>

    <?php // echo $form->field($model, 'foods_in_ticket_created_at') ?>

    <?php // echo $form->field($model, 'foods_in_ticket_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
