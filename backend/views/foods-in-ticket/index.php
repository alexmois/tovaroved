<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FoodsInTicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Foods In Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foods-in-ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Foods In Ticket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'food_in_ticket_id',
            'ticket_id',
            'category_id',
            'food_id',
            'package_id',
            // 'food_in_ticket_count',
            // 'foods_in_ticket_created_at',
            // 'foods_in_ticket_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
