<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\FoodCategories;
use yii\helpers\ArrayHelper;
use backend\models\Foods;
use backend\models\FoodsInTicket;

/* @var $this yii\web\View */
/* @var $model backend\models\FoodsInTicket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foods-in-ticket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ticket_id')->textInput() ?>

    
    
    <?= $form->field($model, 'category_id')->dropDownList(
		ArrayHelper::map(FoodCategories::find()->all(),'category_id','category_title'),
		[
		'prompt'=>'Select categoies',
		'onchange'=>'
			$.post( "index.php?r=foods/lists&id='.'" +$(this).val(), function(data)
			{
				$("select#FoodsInTicket-food_id").html(data);
				});'
		]
	);  ?>
    
    
        <?= $form->field($model, 'food_id')->dropDownList(
   		ArrayHelper::map(Foods::find()->all(),'food_id','food_title'),
    ['prompt'=>'Select food ']
    ) ?>  

    <?= $form->field($model, 'food_id')->textInput() ?>

    <?= $form->field($model, 'package_id')->textInput() ?>

    <?= $form->field($model, 'food_in_ticket_count')->textInput() ?>

    <?= $form->field($model, 'foods_in_ticket_created_at')->textInput() ?>

    <?= $form->field($model, 'foods_in_ticket_updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
