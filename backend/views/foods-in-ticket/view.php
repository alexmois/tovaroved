<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\FoodsInTicket */

$this->title = $model->food_in_ticket_id;
$this->params['breadcrumbs'][] = ['label' => 'Foods In Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foods-in-ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->food_in_ticket_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->food_in_ticket_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'food_in_ticket_id',
            'ticket_id',
            'category_id',
            'food_id',
            'package_id',
            'food_in_ticket_count',
            'foods_in_ticket_created_at',
            'foods_in_ticket_updated_at',
        ],
    ]) ?>

</div>
