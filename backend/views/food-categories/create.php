<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\FoodCategories */

$this->title = 'Create Food Categories';
$this->params['breadcrumbs'][] = ['label' => 'Food Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="food-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
