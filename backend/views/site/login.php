<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="login-box">
      <div class="login-logo">
        <a href="/"><b>ТОВАРО</b>ВЕД</a>        
      </div><!-- /.login-logo -->
      <small>Версия RC1</small>
      <div class="login-box-body">
        <p class="login-box-msg">Войдите в систему</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username', ['options'=>[
                        'tag'=>'div',
                        'class'=>'form-group field-loginform-username has-feedback required'
                        ],
                        'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    {error}{hint}'
                ])->textInput(['placeholder'=>'логин']) ?>

                <?= $form->field($model, 'password', ['options'=>[
                        'tag'=>'div',
                        'class'=>'form-group field-loginform-password has-feedback required'
                        
                        ],
                        'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    {error}{hint}'
                ])->passwordInput(['placeholder' => 'пароль']) ?>

                <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомнить меня'); ?>

                <div class="form-group">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary btn-flat btn-block', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
