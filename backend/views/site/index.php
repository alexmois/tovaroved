<?php

/* @var $this yii\web\View */

use dosamigos\chartjs\ChartJs;
use miloschuman\highcharts\Highcharts;

$this->title = 'ТОВАРОВЕД';
?>
<div class="row">
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-yellow">
	    <div class="inner">
	      <p>Подано</p>
	      <h3><?=$mainStatistic->confirmed?></h3>
	      <small>заявок</small>
	    </div>
	    <div class="icon">
	      <i class="ion ion-bag"></i>
	    </div>
	   
	  </div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-blue">
	    <div class="inner">                  
	      <p>Выполняется</p>
	      <h3><?=$mainStatistic->processed?></h3>
	      <small>заявок</small>
	    </div>
	    <div class="icon">
	      <i class="ion ion-stats-bars"></i>
	    </div>
	   
	  </div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-green">
	    <div class="inner">                  
	      <p>Выполненные</p>
	      <h3><?=$mainStatistic->done?></h3>
	      <small>заявок</small>
	    </div>
	    <div class="icon">
	      <i class="ion ion-person-add"></i>
	    </div>               
	  </div>
	</div><!-- ./col -->
	<div class="col-lg-3 col-xs-6">
	  <!-- small box -->
	  <div class="small-box bg-red">
	    <div class="inner">                  
	      <p>Заканчиваются</p>
	      <h3><?=$mainStatistic->tomorrowEnd?></h3>
	      <small>заявок</small>
	    </div>
	    <div class="icon">
	      <i class="ion ion-pie-graph"></i>
	    </div>               
	  </div>
	</div><!-- ./col -->
	</div>
	<div class="row">
	<div class="col-md-6">

	  <div class="box box-danger">
	    <div class="box-header with-border">
	      <h3 class="box-title">Потоки заявок</h3>
	      <div class="box-tools pull-right">
	        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	      </div>
	    </div>
	    <div class="box-body">
		    <div class="chart">
		         <?= Highcharts::widget([
				   'options' => [
				      'title' => false,
				      'chart' => ['type' => 'area'],
				      'xAxis' => [
				         'categories' => $flowChart->labels,
				         'title' => ['text' => 'Дата']
				      ],
				      'yAxis' => [
				         'title' => ['text' => 'Количество']
				      ],
				      
				      'series' => [
				         ['name' => 'Создано заявок', 'data' => $flowChart->data]
				         
				      ]
				   ]
				]);
				?>
		    </div>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	  

	
	</div><!-- /.col (LEFT) -->
	<div class="col-md-6">
	  	  
	  <!-- BAR CHART -->
	  <div class="box box-success">
	    <div class="box-header with-border">
	      <h3 class="box-title">Доли заказываемых товаров</h3>
	      <div class="box-tools pull-right">
	        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	      </div>
	    </div>
	    <div class="box-body">
	      <div class="chart">	     
          <?= Highcharts::widget([
				   'options' => [
				      'title' => false,
				      'chart' => ['plotBackgroundColor' => '#FFF',
				      			  'plotBorderWidth' => '0px',
				      			  'plotShadow' => false,
				      			  'type' => 'pie'
				      			  ],
				      'tooltip' => [
				            'pointFormat' => '{series.name}: <b>{point.y}</b> раз'
				        ],
				      'plotOptions' => [
				         'pie' =>
				         [
					      	'allowPointSelect' => 'true',
					      	'cursor' => 'pointer',   
					      	'dataLabels' =>
					      	[
						      	'enabled' => 'true',						      	
						      	'style' => 
						      	[
							      	'color' => '#000'
						      	] 						      	
					      	]
				         ]
				      ],
				       'series' => [[					    
			            'name' => 'Заказано',
			            'colorByPoint' => 'true',
			            'data' => $popularFoods
			        ]] 
				   ]
				]);
				?>
		  	
	      </div>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->
	  	
	</div><!-- /.col (RIGHT) -->
</div>
<?	
		
	
	
	?>
	
