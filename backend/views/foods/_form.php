<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\FoodCategories;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Foods */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foods-form">

    <?php $form = ActiveForm::begin(); ?>
    
 
    
    
    <?= $form->field($model, 'food_category_id')->widget(Select2::classname(), [
	    'data' => ArrayHelper::map(FoodCategories::find()->all(),'category_id','category_title'),
	    'language' => 'en',
	    'options' => ['placeholder' => 'Select a state ...'],
	    'pluginOptions' => [
	        'allowClear' => true
	    ],
	]); ?>
    
 	<?= $form->field($model, 'food_articulus')->textInput(['maxlength' => true]) ?>
 	
    <?= $form->field($model, 'food_title')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'food_description')->textarea(['rows' => 6]) ?>

 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
