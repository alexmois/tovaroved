<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FoodsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foods-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>



    <?= $form->field($model, 'food_id') ?>

    <?= $form->field($model, 'food_category_id') ?>
    
    <?= $form->field($model, 'food_articulus') ?>

    <?= $form->field($model, 'food_title') ?>

    <?= $form->field($model, 'food_description') ?>

    <?= $form->field($model, 'food_created_at') ?>

    <?php // echo $form->field($model, 'food_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
