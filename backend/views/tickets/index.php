<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\grid\EditableColumn;

use yii\bootstrap\Modal;
use yii\helpers\Url;



/* @var $this yii\web\View */
/* @var $searchModel backend\models\TicketsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список заявок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="tickets-index">
		<div class="col-lg-6">
	    	<h1 style="margin-top: 0px;"><?= Html::encode($this->title) ?></h1>    
		</div>
	    <!-- проверка прав для создания заявки -->
	    <? 
		if(Yii::$app->user->can( 'tickets_create'))
		  {?>
	    	<div class="col-lg-6"><?= Html::a('Создать заявку', ['create'], ['class' => 'btn btn-success btn-sm pull-right col-lg-3 col-md-2 col-sm-12 col-xs-12']) ?></div>    
	    <?}?>
	    
			
		    <? if(Yii::$app->user->can( 'tickets_delete'))
			{
				$buttonDelete = ['class' => 'yii\grid\ActionColumn',
			            'buttons'=>[
			            		'delete' => function ($url, $model, $key) {
			            			
												
													return Html::a('<i class="fa fa-trash"></i> Удалить', 
																	$url,['class' => 'btn btn-danger btn-xs',
																			'data' => [
																					            'confirm' => 'Are you sure you want to delete this item?',
																					            'method' => 'post',
																				        ],	
																		  ]) ;
											    }
			            ],
		             'template' => '{delete}'];
			}
			else
			{
				$buttonDelete =  ['class' => 'yii\grid\ActionColumn', 'visible' => false];
			}
			?>
			
			<?php \yii\widgets\Pjax::begin(); ?>
			<div class="col-lg-12">
				<br><br>
				<?= GridView::widget([
			        'dataProvider' => $dataProvider,
			        
					'rowOptions'=>function($model)
					{										
						$class=str_replace('btn', '', $model->ticketStatus->status_label);
						$class=str_replace('-', '', $class);
							
						return['class'=>$class];
					},
					
			        'columns' => [
			            ['class' => 'yii\grid\SerialColumn'],
			           
			            [
			               'attribute'=>'ticket_id',
			               'label'=>'Заявка №',
			               'value'=>function ($data) {
						        return $data->ticket_id;
						    },
			            ],
			            
			            [
			               'attribute'=>'ticket_title',
			               'label'=>'Название',
			               'value'=>function ($data) {
						        return $data->ticket_title;
						    },
			            ],
			
						[
			               'attribute'=>'ticketStatus.status_title',
			               'value'=> 'ticketStatus.status_title',
			               'format' => 'raw',
						   'value'=>function ($data) {
							    
						        return Html::tag('span',$data->ticketStatus->status_title,
						        					['class'=>$data->ticketStatus->status_label,'style'=>'width: 100%;']
						        				);
						    },
			               'label'=> 'Статус',
			               'contentOptions' => ['class'=>'text-center']
			            ],
			
			            
						[
			               'attribute'=>'ticket_creator_id',
			               'value'=> 'ticketCreator.username',
			               'label'=>'Отправитель',
			               'value'=>function ($data) {
						        return $data->ticketCreator->first_name." ".$data->ticketCreator->last_name;
						    },
			               
			            ],
			            
						[
			               'attribute'=>'ticket_recepient_id',
			               'value'=> 'ticketRecepient.username',
			               'label'=> 'Получатель',
			               'value'=>function ($data) {
						        return $data->ticketRecepient->first_name." ".$data->ticketRecepient->last_name;
						    },
			
			            ],
			                        
			                        
			            [
			               'attribute'=>'ticket_create_at',
			               'label'=> 'Дата создания',
			               'value'=>function ($data) {
						        return Yii::$app->formatter->asDatetime($data->ticket_created_at, "php:d.m.Y");
						    },
			            ],
			            
			            [
			               'attribute'=>'ticket_end_at',
			               'label'=> 'Дата поставки',
			               'value'=>function ($data) {
						        return Yii::$app->formatter->asDatetime($data->ticket_end_at, "php:d.m.Y");
						    },
			            ],
			             
			           
			            ['class' => 'yii\grid\ActionColumn',
				            'buttons'=>[
				                  'view' => function ($url, $model, $key) {
														return Html::a('<i class="fa fa-eye"></i> Просмотр', 
																		$url,['class' => 'btn btn-primary btn-xs']) ;
												    },
				            ],
			             'template' => '{view}'],
			          	$buttonDelete  
			        ],
			    ]); ?>
			</div>
			<?php \yii\widgets\Pjax::end(); ?>
		
		
	
	</div>
</div>
