<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\User;
use kartik\select2\Select2;
use backend\models\FoodsInTicket;
use yii\grid\GridView; 
use backend\models\Foods;
use backend\models\TicketStatuses;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Tickets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tickets-form col-lg-4">
	
    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data','class'=>'form'],
	    							 'fieldConfig' => [
													        'template' => '{input}'
													        
													    ],
									 ]); ?>

    <?= $form->field($model, 'ticket_title')->textInput(['maxlength' => true,'placeholder'=>'Название заявки'])->label(false); ?>

    
    <?= $form->field($model, 'ticket_recepient_id')->widget(Select2::classname(), [
				'data' => ArrayHelper::map(User::find()->all(),'id',function($model, $defaultValue) {
		        return $model->first_name.' '.$model->last_name;
		    }),
				    'language' => 'ru',
				    'options' => ['placeholder' => 'Выбрать получателя'],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]); ?>
    
  

    
    <?= $form->field($model, 'ticket_end_at')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         'options' => ['placeholder' => 'Дата поставки'],
         
         'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-m-d',
            'startDate' => date('Y-m-d'),
            'todayHighlight' => true,           
            
        ]
])->label(false);?>
    
    <?= Html::submitButton($model->isNewRecord ? 'Создать заявку' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    

    <?php ActiveForm::end(); ?>

</div>
