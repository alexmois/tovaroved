<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use backend\models\FoodsInTicket;
use backend\models\Tickets;
use backend\models\Foods;
use backend\models\Users;
use backend\models\Packages;
use backend\models\TicketsSearch;
use yii\web\Controller;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\User;
use kartik\select2\Select2;
use yii\grid\GridView;
use backend\models\TicketStatuses;
use backend\models\FoodCategories;
//use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Tickets */

$this->title = 'Заявка №'.$model->ticket_id.' - '.$model->ticket_title;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => $model->ticket_id, 'url' => ['view', 'id' => $model->ticket_id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="row">
	<div class="col-lg-4">
		<div class="col-lg-12">
			<h1><?= Html::encode($this->title) ?></h1>		
		</div>	
		
		<div class="tickets-view col-lg-12 col-md-12">
		    <?=
			    DetailView::widget([
			         'model' => $model,
			         'attributes' => [
			            
			            [
				             'attribute' => 'ticketCreator.username',
				             'format'=>'raw',
				             'value'=> $model->ticketCreator->first_name." ".$model->ticketCreator->last_name,
				             
				        ],
			            [
				             'attribute' => 'ticketRecepient.username',
				             'format'=>'raw',
				             'value'=> $model->ticketRecepient->first_name." ".$model->ticketRecepient->last_name,
				             
				        ],
				        [
				             'attribute' => 'Комментарий',
				             'format'=>'raw',
				             'value'=> $model->ticket_comment,
				             
				        ],
			            [
				             'attribute' => 'Статус заявки',
				             'format'=>'raw',
				             'value'=> '<span class="'.$model->ticketStatus->status_label.'">'.$model->ticketStatus->status_title.'</span>',
				             
				        ],
				        [
				             'attribute' => 'Дата создания',
				             'format'=>'raw',
				             'value'=> Yii::$app->formatter->asDatetime($model->ticket_created_at, "php:d.m.Y"),
				             
				        ],
				        [
				             'attribute' => 'Дата поставки',
				             'format'=>'raw',
				             'value'=> Yii::$app->formatter->asDatetime($model->ticket_end_at, "php:d.m.Y"),
				             
				        ],
			            
			        ],
			    ]) ;	    
		    ?> 
		     
		   			 
		</div>
		
		<div class="col-lg-12">
		<!-- скрытие кнопки подачи заявки     -->
			<?php 
		if(Yii::$app->user->can( 'tickets_confirm'))
		{
			?>
			<?=  Html::a('<i class="fa fa-paper-plane"></i> Подать заявку', 		   
			   	['confirm', 'id' => $model->ticket_id], 
				   	[
				        'class' => 'btn btn-success btn-sm',
				        'data' => [			           
				            'method' => 'post'
			        ]
			    ]); 
			?>
			
			<?php } ?>
			
			<!-- скрытие кнопк удаления заявки -->
			
			<?php 
		if(Yii::$app->user->can( 'tickets_delete'))
		{
			?>
			<?= Html::a('<i class="fa fa-trash"></i> Удалить заявку', 		   
			   	['delete', 'id' => $model->ticket_id], 
				   	[
				        'class' => 'btn btn-danger btn-sm',
				        'data' => [
				            'confirm' => 'Are you sure you want to delete this item?',
				            'method' => 'post',
			        ],
			    ]); 
			?>	
			<?php } ?>
		</div>
		<!--  скрытие комментария и обновление статуса -->
		<?php 
		if(Yii::$app->user->can( 'tickets_update'))
		{
			?>
		
		<div class="col-lg-12">
		    <br>
		    <div class="tickets-form">
	
			    <?php $form = ActiveForm::begin(['fieldConfig' => 
				    										[
														        'template' => '{input}'													        
														    ],
												 'action' => Url::toRoute(['tickets/update','id'=>$model->ticket_id])
												]); ?>
				
				
				
			    <?= $form->field($model, 'ticket_comment')->textarea([
				    	'rows' => 6,
				    	'placeholder'=>'Комментарий к заявке'
				    ]); ?>
					
			     <div class="form-group">
			     
			     <?= $form->field($model, 'ticket_status_id')->widget(Select2::classname(), [
				    'data' => ArrayHelper::map(TicketStatuses::find()->all(),'status_id','status_title'),
				    'language' => 'ru',
				    'options' => ['placeholder' => 'Выбрать статус'],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]); ?>
				
				   
								    
			        <?= Html::submitButton('Обновить статус', ['class' => 'btn btn-primary btn-sm']) ?>
					
			     </div>
			   
			
			    <?php ActiveForm::end(); ?>
			
			</div>
		</div>	
		
		<?php } ?>
		
	</div>
	<div class="col-lg-8">

 		<!-- скрытие блока для добавления позиций в заявку     -->
			<?php 
		if(Yii::$app->user->can( 'tickets_confirm'))
		{
			?>
			
		<div class="foods-in-ticket-form col-lg-12">
			<h3>Позиции в заявке</h3>
			
			    <?php $form = ActiveForm::begin(['options'=>['class'=>'form'],
				    							 'fieldConfig' => [
															        'template' => '{input}'
															        
															    ],
				    							 'action' => Url::toRoute('foods-in-ticket/create')]); ?>
			
			
			<?= $form->field($modelFoodsInTicket, 'category_id')->widget(Select2::classname(), [
				    'data' => ArrayHelper::map(FoodCategories::find()->all(),'category_id','category_title'),
				    'language' => 'ru',
				    'options' => [
				    'placeholder' => 'Выбрать категорию',
				    'onchange'=>'
						$.post( "index.php?r=foods/list&id='.'" +$(this).val(), function(data)
						{
							$("select#foodsinticket-food_id").html(data);
							});'
							],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]); ?>
				
			    
				
			   	<?= $form->field($modelFoodsInTicket, 'food_id')->widget(Select2::classname(), [
				    'language' => 'en',
				    'options' => ['placeholder' => 'Выбрать позицию'],
				    'pluginOptions' => [
				        'allowClear' => true,
				        
				    ],
				]); ?> 
				
				<?= $form->field($modelFoodsInTicket, 'package_id')->widget(Select2::classname(), [
				    'data' => ArrayHelper::map(Packages::find()->all(),'package_id','package_title'),
				    'language' => 'ru',
				    'options' => ['placeholder' => 'Тип поставки'],
				    'pluginOptions' => [
				        'allowClear' => true
				    ],
				]); ?>
				
			    
				
		    	<?= $form->field($modelFoodsInTicket, 'food_in_ticket_count')->textInput(['placeholder'=>'Необходимое количество'])->label(false) ?>
				
				<?= Html::submitButton('Добавить позицию', ['class' => 'btn btn-success btn-sm']) ?>
				
		    
		    <?php ActiveForm::end(); ?>	
		</div>
		
		<?php } ?>
		
		<!-- проверка прав для кнопки удаления позиций -->
		<? if(Yii::$app->user->can( 'foods-in-ticket_delete'))
		
		$buttonDelete = ['class' => 'yii\grid\ActionColumn',
			              'buttons'=>[
			                  'view' => function ($url, $model, $key) {
							  					return Html::a('<i class="fa fa-eye"></i>', $url,['class' => 'btn btn-primary btn-xs']) ;
		    							},
		    				  'delete' => function ($url, $model, $key) {
							  					return Html::a('<i class="fa fa-trash"></i>', $url,['class' => 'btn btn-danger btn-xs',
							  																				'data-method' => 'post',
							  																				'hint'=>'Вудуеу',
							  																				'data-confirm' => Yii::t('yii', 'Удалить позицию из заявки?'),]) ;
		    							},
									 ],
						 'controller' => 'foods-in-ticket',
			             'template'=>'{delete}'];
						 
else
	{
		$buttonDelete =  ['class' => 'yii\grid\ActionColumn', 'visible' => false];
	}
		?>
		
		<div class="col-lg-12">		
			<hr>
		</div>
		<div class="foods-in-ticket-index col-lg-12">
			<div class="table-responsive">
			    <?= GridView::widget([
			        'dataProvider' => $dataProvider,		       
			        'columns' => [
			            ['class' => 'yii\grid\SerialColumn'],
			            [
				          'attribute'=>'category.category_title',
			              'label'=>'Категория',
			              'value'=>'category.category_title' 
			            ],
			            [
				          'attribute'=>'food.food_title',
			              'label'=>'Позиция',
			              'value'=>function ($data) {
								        return $data->food->food_articulus.' - '.$data->food->food_title;								        
								    },
			            ],
			            [
				          'attribute'=>'food_in_ticket_count',
			              'label'=>'Количество',
			              'value'=>'food_in_ticket_count' 
			            ],
			            [
				          'attribute'=>'package.package_title',
			              'label'=>'Упаковка',
			              'value'=>'package.package_title' 
			            ],
			            
			           $buttonDelete 
			        ],
			    ]); ?>
			</div>
	
		</div>
	</div>
</div>
