<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "foods_in_ticket".
 *
 * @property integer $food_in_ticket_id
 * @property integer $ticket_id
 * @property integer $category_id
 * @property integer $food_id
 * @property integer $package_id
 * @property integer $food_in_ticket_count
 * @property string $foods_in_ticket_created_at
 * @property string $foods_in_ticket_updated_at
 *
 * @property FoodCategories $category
 * @property Tickets $ticket
 * @property Foods $food
 * @property Packages $package
 */
class FoodsInTicket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foods_in_ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'category_id', 'food_id', 'package_id', 'food_in_ticket_count', 'foods_in_ticket_created_at', 'foods_in_ticket_updated_at'], 'required'],
            [['ticket_id', 'category_id', 'food_id', 'package_id', 'food_in_ticket_count'], 'integer'],
            [['foods_in_ticket_created_at', 'foods_in_ticket_updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'food_in_ticket_id' => 'Food In Ticket ID',
            'ticket_id' => 'Ticket ID',
            'category_id' => 'Category ID',
            'food_id' => 'Food ID',
            'package_id' => 'Package ID',
            'food_in_ticket_count' => 'Food In Ticket Count',
            'foods_in_ticket_created_at' => 'Foods In Ticket Created At',
            'foods_in_ticket_updated_at' => 'Foods In Ticket Updated At'
        ];
    }
    


    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'food_title', 'food_count'
        ]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FoodCategories::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Tickets::className(), ['ticket_id' => 'ticket_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFood()
    {
        return $this->hasOne(Foods::className(), ['food_id' => 'food_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Packages::className(), ['package_id' => 'package_id']);
    }
    
    
}
