<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Foods;

/**
 * FoodsSearch represents the model behind the search form about `backend\models\Foods`.
 */
class FoodsSearch extends Foods
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['food_id', 'food_articulus'], 'integer'],
            [['food_title', 'food_category_id', 'food_description', 'food_created_at', 'food_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Foods::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
		$query->joinWith('foodCategory');

        $query->andFilterWhere([
            'food_id' => $this->food_id,
            
            'food_created_at' => $this->food_created_at,
            'food_updated_at' => $this->food_updated_at,
        ]);

        $query->andFilterWhere(['like', 'food_title', $this->food_title])
            ->andFilterWhere(['like', 'food_description', $this->food_description])
			->andFilterWhere(['like', 'food_categories.category_title', $this->food_category_id])
			->andFilterWhere(['like', 'food_articulus', $this->food_articulus]);

        return $dataProvider;
    }
}
