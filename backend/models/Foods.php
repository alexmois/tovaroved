<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "foods".
 *
 * @property integer $food_id
 * @property integer $food_category_id
 * @property string $food_title
 * @property string $food_description
 * @property string $food_created_at
 * @property string $food_updated_at
 *
 * @property FoodCategories $foodCategory
 * @property FoodsInTicket[] $foodsInTickets
 */
class Foods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['food_category_id', 'food_articulus', 'food_title', 'food_description', 'food_created_at', 'food_updated_at'], 'required'],
            [['food_category_id','food_articulus'], 'integer'],
            [['food_description'], 'string'],
            [['food_created_at', 'food_updated_at'], 'safe'],
            [['food_title'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'food_id' => 'Food ID',
            'food_category_id' => 'Food Category ID',
            'food_articulus' => 'Food Articulus',
            'food_title' => 'Food Title',
            'food_description' => 'Food Description',
            'food_created_at' => 'Food Created At',
            'food_updated_at' => 'Food Updated At',
        ];
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodCategory()
    {
        return $this->hasOne(FoodCategories::className(), ['category_id' => 'food_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodsInTickets()
    {
        return $this->hasMany(FoodsInTicket::className(), ['food_id' => 'food_id']);
    }
}
