<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property integer $ticket_id
 * @property string $ticket_title
 * @property integer $ticket_creator_id
 * @property integer $ticket_recepient_id
 * @property string $ticket_comment
 * @property string $ticket_comment_update_date
 * @property integer $ticket_status_id
 * @property string $ticket_created_at
 * @property string $ticket_end_at
 * @property string $ticket_updated_at
 *
 * @property FoodsInTicket[] $foodsInTickets
 * @property TicketStatuses $ticketStatus
 * @property User $ticketCreator
 * @property User $ticketRecepient
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_title', 'ticket_recepient_id', 'ticket_end_at'], 'required'],  //, 'ticket_comment', 'ticket_comment_update_date', 'ticket_created_at', 'ticket_creator_id', 'ticket_updated_at'
            [['ticket_creator_id', 'ticket_recepient_id', 'ticket_status_id'], 'integer'],
            [['ticket_comment'], 'string'],
            [['ticket_comment_update_date', 'ticket_created_at', 'ticket_end_at', 'ticket_updated_at'], 'safe'],
            [['ticket_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ticket_id' => 'Ticket ID',
            'ticket_title' => 'Название',
            'ticketCreator.username' => 'Создатель',
            'ticketRecepient.username' => 'Получатель',
            'ticket_comment' => 'Ticket Comment',
            'ticket_comment_update_date' => 'Ticket Comment Update Date',
            'ticket_status_id' => 'Статус',
            'ticket_created_at' => 'Создано',
            'ticket_end_at' => 'Конец',
            'ticket_updated_at' => 'Обновлено',
           
        ];
    }
    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodsInTickets()
    {
        return $this->hasMany(FoodsInTicket::className(), ['ticket_id' => 'ticket_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketStatus()
    {
        return $this->hasOne(TicketStatuses::className(), ['status_id' => 'ticket_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'ticket_creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketRecepient()
    {
        return $this->hasOne(User::className(), ['id' => 'ticket_recepient_id']);
    }
}
