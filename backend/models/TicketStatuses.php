<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ticket_statuses".
 *
 * @property integer $status_id
 * @property string $status_title
 *
 * @property Tickets[] $tickets
 */
class TicketStatuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_title'], 'required'],
            [['status_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'Status ID',
            'status_title' => 'Status Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Tickets::className(), ['ticket_status_id' => 'status_id']);
    }
}
