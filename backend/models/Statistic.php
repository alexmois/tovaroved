<?php

namespace backend\models;

use Yii;

/**
 * StatisticModel present statistic data for visualisation
 */

class Statistic extends \yii\base\Model
{
    
    /**
     * Find count of Tickets statuses for actionIndex in SiteController
     * @return Object with (confirmed, processed, done and tomorrowEnd) counts
     */
    public function getMainStatistic()
    {
	    $mainStatistic = new \stdClass;
	    
	    $mainStatistic->confirmed = Tickets::find()
					       ->where(['ticket_status_id' => 2])
					       ->count();
		
		$mainStatistic->processed = Tickets::find()
					       ->where(['ticket_status_id' => 3])
					       ->count();
					       
		$mainStatistic->done = Tickets::find()
					       ->where(['ticket_status_id' => 4])
					       ->count();
		
		$mainStatistic->tomorrowEnd = Tickets::find()
					       ->where(['<>','ticket_status_id',4])
					       ->andWhere(['ticket_end_at' => date('Y-m-d').'+ INTERVAL 1 DAY'])
					       ->count();
					       
        return $mainStatistic;
    }
    
    /**
     * Take data count for chart building in date satisfaction
     * @param array $dateFrom (Y-d-m)
     * @param array $dateFrom (Y-d-m)
     * @return Object with (date, count) counts
     */
    public function getTicketFlow($dateFrom, $dateTo)
    {
	 	$ticketsFlow = Tickets::find()
	 					   ->select([
	 					   			 'COUNT(ticket_id) as ticket_id',
	 					   			 'DATE_FORMAT(ticket_created_at,"%d.%m.%Y") as ticket_created_at'
	 					   			])
					       ->where(['>=','ticket_created_at',$dateFrom])
					       ->andWhere(['<=','ticket_created_at',$dateTo])
					       ->groupBy(['DATE_FORMAT(ticket_created_at,"%Y-%m-%d")'])					       
						   ->all(); 
						   
		if(count($ticketsFlow)>0)
		{
			$flowChart = new \stdClass();
			
			foreach($ticketsFlow as $flow)
			{
				$flowChart->labels[] = $flow->ticket_created_at;
				$flowChart->data[] = $flow->ticket_id;
			}
			return $flowChart;	
		}
		else
		{
			return $ticketsFlow;
		}
    }
    
    /**
     * Take data foods for donut chart building in date satisfaction
     * 
     * @param array $dateFrom (Y-d-m)
     * @param array $dateFrom (Y-d-m)
     *
     * @return Object with (food title, count) counts
     */
    public function getPopularFoods($dateFrom,$dateTo)
    {
	 	$popularFoods = FoodsInTicket::find()	 					   
	 					   	->select(['foods.food_title as name','COUNT(foods_in_ticket.food_in_ticket_id) as y'])
						    ->leftJoin('foods', '`foods`.`food_id` = `foods_in_ticket`.`food_id`')
						    ->where(['>=','foods_in_ticket_created_at',$dateFrom])
					        ->andWhere(['<=','foods_in_ticket_created_at',$dateTo])
						    ->groupBy('`foods`.`food_id`')		
						    ->asArray()				   
						    ->all();	
		
		foreach($popularFoods as $key => $food)
		{
			foreach($food as $param => $value)
			{
				if($param=='y')
				{
					$popularFoods[$key][$param]=(int)$value;	
				}	
			}	
		}
		return $popularFoods;	
			
    }  
}
