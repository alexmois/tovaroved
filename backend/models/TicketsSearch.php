<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Tickets;


/**
 * TicketsSearch represents the model behind the search form about `backend\models\Tickets`.
 */
class TicketsSearch extends Tickets
{
    /**
     * @inheritdoc
     */
    public $globalSearch;
     
     
    public function rules()
    {
        return [
            [['ticket_id', 'ticket_creator_id', 'ticket_recepient_id', 'ticket_status_id'], 'integer'],
            [['ticket_title', 'globalSearch', 'ticket_comment', 'ticket_comment_update_date', 'ticket_created_at', 'ticket_end_at', 'ticket_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tickets::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                 'forcePageParam' => false,
                 'pageSizeParam' => false,
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->orFilterWhere([
            'ticket_id' => $this->ticket_id,
            'ticket_creator_id' => $this->ticket_creator_id,
            'ticket_recepient_id' => $this->ticket_recepient_id,
            'ticket_comment_update_date' => $this->ticket_comment_update_date,
            'ticket_status_id' => $this->ticket_status_id,
            'ticket_created_at' => $this->ticket_created_at,
            'ticket_end_at' => $this->ticket_end_at,
            'ticket_updated_at' => $this->ticket_updated_at,
        ]);

        $query->orFilterWhere(['like', 'ticket_title', $this->globalSearch])
            ->orFilterWhere(['like', 'ticket_id', $this->globalSearch])
			->orFilterWhere(['like', 'ticket_creator_id', $this->globalSearch])
			
			
			
			->orFilterWhere(['like', [
               'attribute'=>'ticket_recepient_id',
               'value'=> 'ticketRecepient.username',
           		 ], $this->globalSearch])
			
			
			
			->orFilterWhere(['like', 'ticket_comment', $this->globalSearch]);
			
		$query->orderBy('ticket_id DESC');
		
        return $dataProvider;
    }
}
