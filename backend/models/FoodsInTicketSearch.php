<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\FoodsInTicket;

/**
 * FoodsInTicketSearch represents the model behind the search form about `backend\models\FoodsInTicket`.
 */
class FoodsInTicketSearch extends FoodsInTicket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['food_in_ticket_id', 'ticket_id', 'category_id', 'food_id', 'package_id', 'food_in_ticket_count'], 'integer'],
            [['foods_in_ticket_created_at', 'foods_in_ticket_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
	    	    
        $query = FoodsInTicket::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if(isset($params['id']))
        {
	    	$query->where('ticket_id='.$params['id']);    
        }

        $query->andFilterWhere([
            'food_in_ticket_id' => $this->food_in_ticket_id,
            'ticket_id' => $this->ticket_id,
            'category_id' => $this->category_id,
            'food_id' => $this->food_id,
            'package_id' => $this->package_id,
            'food_in_ticket_count' => $this->food_in_ticket_count,
            'foods_in_ticket_created_at' => $this->foods_in_ticket_created_at,
            'foods_in_ticket_updated_at' => $this->foods_in_ticket_updated_at,
        ]);

        return $dataProvider;
    }
}
