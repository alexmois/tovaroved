<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "food_categories".
 *
 * @property integer $category_id
 * @property string $category_title
 * @property string $category_description
 * @property string $category_created_at
 * @property string $category_updated_at
 *
 * @property Foods[] $foods
 */
class FoodCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'food_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_title', 'category_description', 'category_created_at', 'category_updated_at'], 'required'],
            [['category_description'], 'string'],
            [['category_created_at', 'category_updated_at'], 'safe'],
            [['category_title'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'category_title' => 'Category Title',
            'category_description' => 'Category Description',
            'category_created_at' => 'Category Created At',
            'category_updated_at' => 'Category Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoods()
    {
        return $this->hasMany(Foods::className(), ['food_category_id' => 'category_id']);
    }
}
