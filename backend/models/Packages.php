<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "packages".
 *
 * @property integer $package_id
 * @property string $package_title
 * @property string $package_created_at
 * @property string $package_updated_at
 *
 * @property FoodsInTicket[] $foodsInTickets
 */
class Packages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'packages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_title', 'package_created_at', 'package_updated_at'], 'required'],
            [['package_created_at', 'package_updated_at'], 'safe'],
            [['package_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'package_id' => 'Package ID',
            'package_title' => 'Package Title',
            'package_created_at' => 'Package Created At',
            'package_updated_at' => 'Package Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodsInTickets()
    {
        return $this->hasMany(FoodsInTicket::className(), ['package_id' => 'package_id']);
    }
}
