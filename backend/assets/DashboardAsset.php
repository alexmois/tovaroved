<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/site.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
        '//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css',
        'plugins/iCheck/flat/blue.css',
        
         'css/morris.css',
         'css/jquery-jvectormap-1.2.2.css',
         'css/datepicker3.css',
         'css/daterangepicker-bs3.css',
         'css/bootstrap3-wysihtml5.min.css',
    ];
    public $js = [
	    'js/bootstrap.min.js',
	    '//code.jquery.com/ui/1.11.4/jquery-ui.min.js',
	    '//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
	    'plugins/sparkline/jquery.sparkline.min.js',
	    'plugins/slimScroll/jquery.slimscroll.min.js',
	    'plugins/fastclick/fastclick.min.js',
	    'js/app.min.js',	    
	    //'js/dashboard.js',
		'js/main.js',
	     //'js/jQuery-2.1.4.min.js',   
	     'js/morris.min.js',
	     'js/jquery-jvectormap-1.2.2.min.js',
	     'js/jquery-jvectormap-world-mill-en.js',
	     'js/jquery.knob.js',
	     'js/daterangepicker.js',
	     'js/bootstrap-datepicker.js',
	     'js/bootstrap3-wysihtml5.all.min.js',
	     'js/demo.js',
	     'plugins/chartjs/Chart.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public $jsOptions = array(
	   // 'position' => \yii\web\View::POS_HEAD
	);
}
