<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use backend\models\Statistic;
use backend\models\SignupForm;
use backend\models\AuthItem;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','signup'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
		$model 				= new Statistic();				
		$dateTo 			= date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d'))));
		$dateFrom 			= date('Y-m-d', strtotime('-1 month', strtotime($dateTo)));		
		$mainStatistic 		= $model->getMainStatistic(); 	
		$flowChart 			= $model->getTicketFlow($dateFrom,$dateTo);		
		$popularFoods 		= $model->getPopularFoods($dateFrom,$dateTo);
							
	    return $this->render('index',
	    	[
		    	'mainStatistic'		=> $mainStatistic,
		    	'flowChart' 		=> $flowChart,
		    	'popularFoods'		=> $popularFoods
	    	]);
    }

    public function actionLogin()
    {   
        $this->layout = 'loginLayout';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	
	/**
     * Signs user up.
     *
     * @return mixed
     */  
	
	public function actionSignup()
    {
        $model = new SignupForm();
        $authItems = AuthItem::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'authItems'=>$authItems,
        ]);
    }
}
