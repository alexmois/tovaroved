<?php

namespace backend\controllers;

use Yii;
use backend\models\Foods;
use backend\models\FoodsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * FoodsController implements the CRUD actions for Foods model.
 */
class FoodsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Foods models.
     * @return mixed
     */
    public function actionIndex()
    {
			
        $searchModel = new FoodsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		if(Yii::$app->request->post('hasEditable'))
        {
            $foodsID = Yii::$app->request->post('editableKey');
            $food = Foods::findOne($foodsID);
            //$branch->save();
            $out = Json::encode(['output'=>'','message'=>'']);
            $post= [];
            $posted = corrent($_POST['Foods']);
            $post['foods'] = $posted; 
            if($food->load($post))
            {
                $food->save();
                $output = 'my values';
                $out = Json::encode(['output'=>$output, 'message'=>'']);
            }
            echo $out;
            return;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		
    }

    /**
     * Displays a single Foods model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
		
		
    }

    /**
     * Creates a new Foods model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    		
        $model = new Foods();

        if ($model->load(Yii::$app->request->post()) ) 
        {
        	$model->food_created_at = date('Y-m-d h:m:s');
			$model->food_updated_at = date('Y-m-d h:m:s');
			$model->save();
			
            return $this->redirect(['view', 'id' => $model->food_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        
    }

    /**
     * Updates an existing Foods model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	
		
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->food_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
		
			
    }

    /**
     * Deletes an existing Foods model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
		
    }


	 /*  Dependent Drop Down Lists  */
	public function actionList($id)
	{
		
		$countFoods = Foods::find()
			->where(['food_category_id'=>$id])
			->count();
		
		if($countFoods > 0)
		{
			$foods = Foods::find()
			->where(['food_category_id'=>$id])
			->orderBy('food_title ASC')
			->all();
			
			echo "<option>Позиция</option>";
			foreach($foods as $food)
			{
				echo "<option value='" .$food->food_id. "'>". $food->food_articulus." - ".$food->food_title."</option>";
				//echo "<option value='" .$food->food_id. "'>".$food->food_title."</option>";
			}
		}
		else 
		{
			echo "<option>Пусто</option>";
		}
		
	}



    /**
     * Finds the Foods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Foods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Foods::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function beforeAction($action)
	{
		$ruleName = $this->uniqueid.'_'.$action->id;
		//print_r($ruleName);
		//die();
		
	    if (parent::beforeAction($action)) {
	        if (!Yii::$app->user->can($ruleName)) {
	            throw new ForbiddenHttpException('Доступ закрыт. У вас недостаточно прав');
	        }
	        return true;
	    } else {
	        return false;
	    }
	}
}
