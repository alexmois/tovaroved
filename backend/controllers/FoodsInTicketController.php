<?php

namespace backend\controllers;

use Yii;
use backend\models\FoodsInTicket;
use backend\models\FoodsInTicketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Session;
use yii\web\ForbiddenHttpException;

/**
 * FoodsInTicketController implements the CRUD actions for FoodsInTicket model.
 */
class FoodsInTicketController extends Controller
{
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FoodsInTicket models.
     * @return mixed
     */
    public function actionIndex()
    {
    	
        $searchModel = new FoodsInTicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FoodsInTicket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FoodsInTicket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	        $model = new FoodsInTicket();

        if ($model->load(Yii::$app->request->post())) {
	        
	        $session = Yii::$app->session;
	        $model->ticket_id = $session->get('ticket_id');
	        $model->foods_in_ticket_created_at = date('Y-m-d H:i:s');
	        $model->foods_in_ticket_updated_at = date('Y-m-d H:i:s');
	        
	        if($model->save())
	        {
		   		return $this->redirect(['tickets/view', 'id' => $model->ticket_id]);     
	        }
	        else
	        {
		    	return $this->render('create', [
	                'model' => $model,
	            ]);    
	        }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FoodsInTicket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->food_in_ticket_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FoodsInTicket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	
        $this->findModel($id)->delete();
        
		$session = Yii::$app->session;
	    $ticket_id = $session->get('ticket_id');
	    
	    return $this->redirect(['tickets/view', 'id' => $ticket_id]);   

    }
  
    /**
     * Finds the FoodsInTicket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FoodsInTicket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FoodsInTicket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
	public function beforeAction($action)
	{
		$ruleName = $this->uniqueid.'_'.$action->id;
		//print_r($ruleName);
		//die();
		
	    if (parent::beforeAction($action)) {
	        if (!Yii::$app->user->can($ruleName)) {
	            throw new ForbiddenHttpException('Доступ закрыт. У вас недостаточно прав');
	        }
	        return true;
	    } else {
	        return false;
	    }
	}
    
}
