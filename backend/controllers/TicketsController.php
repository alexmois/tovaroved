<?php

namespace backend\controllers;

use Yii;
use backend\models\FoodsInTicket;
use backend\models\Tickets;
use backend\models\TicketsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\FoodsInTicketSearch;
use yii\db\Query;
use yii\web\Session;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\data\Pagination;


/**
 * TicketsController implements the CRUD actions for Tickets model.
 */
class TicketsController extends Controller
{
    public function behaviors()
    {
        return [
	        'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','create','view','update','delete','confirm'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tickets models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new TicketsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tickets model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	
    	$session = Yii::$app->session;
    	
    	$model = $this->findModel($id);
		
		$foodsinticket = new FoodsInTicket();
		
		$searchModel = new FoodsInTicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$session->set('ticket_id', $model->ticket_id);
		
		return $this->render('view', [
                'model' => $model,
                'modelFoodsInTicket' => $foodsinticket,
                'searchModel' => $searchModel,
            	'dataProvider' => $dataProvider,
             
            ]);
    }

    /**
     * Creates a new Tickets model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	
        $model = new Tickets();
		//$foodsinticket = new FoodsInTicket();

        if ($model->load(Yii::$app->request->post())) {
        	$model->ticket_creator_id = Yii::$app->user->id;	
			$model->ticket_created_at = date('Y-m-d h:m:s');	
				
			if($model->save())
			{				
            	return $this->redirect(['view', 'id' => $model->ticket_id]);
			}
			else
			{
					 return $this->render('create', [
		                'model' => $model,
		                //'foodsinticket' => $foodsinticket,
		            ]);	
			}
			
        } else {
            return $this->render('create', [
                'model' => $model,
                //'foodsinticket' => $foodsinticket,
            ]);
        }
    }

    /**
     * Updates an existing Tickets model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	
	   $model = $this->findModel($id); 
	   
	   if ($model->load(Yii::$app->request->post())) {
		   $model->ticket_comment_update_date = date('Y-m-d H:i:s');
	       $model->save();
           return $this->redirect(['view', 'id' => $model->ticket_id]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }    
    
    /**
     * Updates an existing Tickets model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionConfirm($id)
    {    	
	   $model = $this->findModel($id); 
	   
	   if ($model) {
		   
		   $model->ticket_status_id = 2;
		   $model->ticket_updated_at = date('Y-m-d H:i:s');
	       $model->save();
	       
           return $this->redirect(['view', 'id' => $model->ticket_id]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Tickets model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    
		
        FoodsInTicket::deleteAll(['ticket_id'=>$id]); 
        if($this->findModel($id)->delete())
        {
	    	return $this->redirect(['index']); 	
     	}
     	else
     	{
	    	return $this->redirect(['view', 'id' => $id]); 	
     	}       	    
    }

    /**
     * Finds the Tickets model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tickets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tickets::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function beforeAction($action)
	{
		$ruleName = $this->uniqueid.'_'.$action->id;
		//print_r($ruleName);
		//die();
		
	    if (parent::beforeAction($action)) {
	        if (!Yii::$app->user->can($ruleName)) {
	            throw new ForbiddenHttpException('Доступ закрыт. У вас недостаточно прав');
	        }
	        return true;
	    } else {
	        return false;
	    }
	}
    
}
